# DS/2 Jersey Media Json GSON

[ ![Download](https://api.bintray.com/packages/dstrauss/ds2-oss/jersey-media-json-gson/images/download.svg) ](https://bintray.com/dstrauss/ds2-oss/jersey-media-json-gson/_latestVersion)

A provider for any Jax RS service using json with Google GSON.

## Build

Invoke Gradle using

    ./gradlew clean build

## Release 

To create a release, you typically use:

    export BINTRAY_API_KEY="abcdef"
    ./gradlew :clean :release

The release mechanism will tell you what to do (interactive release!!).